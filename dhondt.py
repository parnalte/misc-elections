# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import numpy as np



def votes_per_candidate(total_votes, total_seats):
    """
    Given the number of votes (or percentage) for a given party and the 
    total no. of seats, create an array in which we 'assign' the individual
    votes for each candidate in the party's list
    """

    divisors = np.arange(total_seats) + 1.0

    return total_votes/divisors
    

def dhondt_simple(dict_votes_parties, total_seats, Verbose=False):
    
    Np = len(dict_votes_parties)    
    party_names = dict_votes_parties.keys()
    
    #Create 'tags' for candidates
    cand_tags = []
    for pname in party_names:
        cand_tags = cand_tags + total_seats*[pname]
        
    #And get all the votes per candidate
    vpc = np.empty(0)
    
    for pname in party_names:
        vpc_this = votes_per_candidate(dict_votes_parties[pname], total_seats)
        vpc = np.concatenate((vpc, vpc_this))
        
    idx_sort = vpc.argsort()[::-1]
    
    if Verbose:
        
        for i in range(total_seats):
            
            print "%d  %.5g  %s" % (i+1, vpc[idx_sort[i]],
                                    cand_tags[idx_sort[i]])
        #Next possible elected candidate
        print "---------------"
        print "%d  %.5g  %s" % (total_seats+1, 
                                vpc[idx_sort[total_seats]],
                                cand_tags[idx_sort[total_seats]])
                                
    #Do the final counting
    party_seats = dict()
    
    for pname in party_names:
        party_seats[pname] = 0
    
    for i in range(total_seats):
        this_seat_tag = cand_tags[idx_sort[i]]
        party_seats[this_seat_tag] +=1
    
    return party_seats

def dhondt(dict_votes_parties, total_seats, perc_min=3.0, Verbose=False):
    """
    Calculate the assignation of seats to parties following the d'Hondt rule.
    We exclude any party with a share below 'perc_min' (in percentage) or
    with the name 'Others' (or similar).
    To apply the limit, we assume *all* the votes to be counted are passed.
    """
    
    other_list = ['Others', 'others', 'Other', 'other', 'Otros', 'otros', 
                  'Otro', 'otro', 'Altres', 'altres', 'Altre', 'altre']
    dict_simplified = dict()
    
    Np = len(dict_votes_parties)    
    party_names = dict_votes_parties.keys()
    party_votes = np.array(dict_votes_parties.values())
    
    total_votes = party_votes.sum()
    votes_min = (perc_min/100.)*total_votes
    
    for pname in party_names:
        if pname in other_list:
            if Verbose:
                print "We exclude party ", pname, " because of name"
        elif dict_votes_parties[pname] < votes_min:
            if Verbose:
                print "We exclude party ", pname, " because of votes threshold"
        else:
            dict_simplified[pname] = dict_votes_parties[pname]
            
    return dhondt_simple(dict_simplified, total_seats, Verbose)
    
    
            
            